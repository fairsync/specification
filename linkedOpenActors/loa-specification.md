[[_TOC_]]

# Introduction

# Context and requirements

# Information exchange data model
  
![information exchange data model](diagrams/loa_information_exchange_data_model.png "information exchange data model diagram")

## Agent
**Typ:**  
[http://xmlns.com/foaf/0.1/Agent](http://xmlns.com/foaf/spec/#term_Agent)   

**Description:**  
Core or Head of our domain is an Agent (eg. person, group, software or physical artifact). 

**Properties:**
- [http://purl.org/dc/terms/identifier](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/identifier)  
Recommended practice is to identify the resource by means of a string conforming to an identification system. Examples include International Standard Book Number (ISBN), Digital Object Identifier (DOI), and Uniform Resource Name (URN). Persistent identifiers should be provided as HTTP URIs.  
**TODO** how to Content_Negotiation ?? in case of kvm or adapters
- [http://xmlns.com/foaf/0.1/name](http://xmlns.com/foaf/spec/#term_name)  
The name of something is a simple textual string.  
- [http://xmlns.com/foaf/0.1/homepage](http://xmlns.com/foaf/spec/#term_homepage)  
A homepage for some thing.   
- [http://www.w3.org/2002/07/owl#versionInfo](https://www.w3.org/TR/owl-ref/#versionInfo-def)  
**NOTE:** _That is not really a good property for that what we need. But i didn't find another yet._
- [http://purl.org/dc/terms/description](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/description)  
Description may include but is not limited to: an abstract, a table of contents, a graphical representation, or a free-text account of the resource.

## Group
**Typ:**  
[http://xmlns.com/foaf/0.1/Group](http://xmlns.com/foaf/spec/#term_Group)

**Description:**  
The Group class represents a collection of individual agents (and may itself play the role of a Agent, ie. something that can perform actions). This concept is intentionally quite broad, covering informal and ad-hoc groups, long-lived communities, organizational groups within a workplace, etc. 

**Properties:**  
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Legal Entity
**Typ:**  
[http://www.w3.org/ns/legal#LegalEntity](https://joinup.ec.europa.eu/site/core_business/rdfs.html#legal:LegalEntity)

**Description:**  
Represents a organisation that is legally registered.

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Person
**Typ:**  
[http://www.w3.org/ns/person#Person](https://joinup.ec.europa.eu/site/core_person/rdfs.html#person:Person)

**Description:**  
An individual person who may be dead or alive, but not imaginary. It is that restriction that makes Person a sub class of both foaf:Person and schema:Person which both cover imaginary characters as well as real people.

## Organizational Unit
**Typ:**  
[http://www.w3.org/ns/org#OrganizationalUnit](https://www.w3.org/TR/vocab-org/#org:OrganizationalUnit)

**Description:**  
An Organization such as a department or support unit which is part of some larger Organization and only has full recognition within the context of that Organization. In particular the unit would not be regarded as a legal entity in its own right. 

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Organizational Collaboration
**Typ:**  
[http://www.w3.org/ns/org#OrganizationalCollaboration](https://www.w3.org/TR/vocab-org/#org:OrganizationalCollaboration)

**Description:**  
A collaboration between two or more Organizations such as a project. It meets the criteria for being an Organization in that it has an identity and defining purpose independent of its particular members but is neither a formally recognized legal entity nor a sub-unit within some larger organization. Might typically have a shorter lifetime than the Organizations within it, but not necessarily.

**Properties:**
- https://schema.org/foundingDate  
The date that this organization was founded.  

## Location
**Typ:**  
[http://purl.org/dc/terms/Location](https://joinup.ec.europa.eu/site/core_location/rdfs.html#dcterms:Location)

**Description:**  
Represents any location, irrespective of size or other restriction.
Unlike the [Business Core Vocabulary](https://joinup.ec.europa.eu/site/core_business/rdfs.html) and [The Organization Ontology](https://www.w3.org/TR/vocab-org) models, we use this location for all agents.

See bottom left on the [poster](https://ec.europa.eu/isa2/sites/isa/files/corevocabularies-poster.pdf)

**Properties**
- Geometry (see following headlines)
- Address (see following headlines)

## Geometry
**Typ:**  
[http://www.w3.org/ns/locn#Geometry](https://joinup.ec.europa.eu/site/core_location/rdfs.html#locn:Geometry)

**Description:**  
The Geometry Class provides the means to identify a Location as a point.

**Properties**
- https://schema.org/latitude  
The latitude of a location. For example 37.42242 [(WGS 84)](https://en.wikipedia.org/wiki/World_Geodetic_System).  
- https://schema.org/longitude  
The longitude of a location. For example -122.08585 [(WGS 84)](https://en.wikipedia.org/wiki/World_Geodetic_System).  

## Address
**Typ:**
[http://www.w3.org/ns/locn#Address](https://joinup.ec.europa.eu/site/core_location/rdfs.html#locn:Address)

**Description:**  
An "address representation"

**Properties**
- https://www.w3.org/ns/locn#locn:postCode  
The post code (a.k.a postal code, **zip code** etc.). Post codes are common elements in many countries' postal address systems.  

- https://www.w3.org/ns/locn#locn:adminUnitL1  
The uppermost administrative unit for the address, almost always a **country**.   

- https://www.w3.org/ns/locn#locn:adminUnitL2  
The region of the address, usually a **county**, state or other such area that typically encompasses several localities.  

- https://www.w3.org/ns/locn#locn:postName  
The key postal division of the address, usually the **city**  

- https://www.w3.org/ns/locn#locn:addressArea  
The name or names of a geographic area or locality that groups a number of addressable objects for addressing purposes, without being an administrative unit. This would typically be part of a city, a neighbourhood or village.  
_Note: As a German, I understand this as a street and would map the street including the house number here for Germany._

## Contact Point
**Typ:**  
https://schema.org/ContactPoint

**Description:**  
A contact point for a person or organization.  
Described in the [Core Public Service Vocabulary Application Profile](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/core-public-service-vocabulary-application-profile/releases) see [pdf](
https://github.com/catalogue-of-services-isa/CPSV-AP/raw/master/releases/2.2.1/SC2015DI07446_D02.02_CPSV-AP-2.2.1_v1.00.pdf) chapter    `3.18. The Contact Point Class`
It references [schema.org ContactPoint](https://schema.org/ContactPoint)
  
**Properties**
- https://schema.org/name  
The name of the item. This could be the name of a contact person.
- https://schema.org/email  
Email address.  
- https://schema.org/telephone  
The telephone number.

# ISA2 Mapping

| ISA<sup>2</sup> Type| ISA<sup>2</sup> Property | LOA Type | LOA Property
| ------ | ------ | ------ |------ |
| cell | cell | cell | cell |
| cell | cell | cell | cell |.


# misc (todo)


## Mark as duplicates
https://www.w3.org/TR/owl-ref/#sameAs-def

## Mark as NOT a duplicate
https://www.w3.org/TR/owl-ref/#differentFrom-def

## To be discussed

### Opening OpeningHoursSpecification
In the [Core Public Service Vocabulary Application Profile 2.2.1](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/core-public-service-vocabulary-application-profile/release/221)

The opening Hour specification of schema.org is used. the pdf is available [here](https://github.com/catalogue-of-services-isa/CPSV-AP/raw/master/releases/2.2.1/SC2015DI07446_D02.02_CPSV-AP-2.2.1_v1.00.pdf) See `3.12.5. Opening Hours`.

The OpeningHoursSpecification of [goodrelations](http://www.heppnetz.de/ontologies/goodrelations/v1.html#OpeningHoursSpecification) is also very simmilar to the [schema.org OpeningHoursSpecification](https://schema.org/OpeningHoursSpecification)

The [openstreetmap format](https://wiki.openstreetmap.org/wiki/Key:opening_hours) currently used by karte von morgen differs in format.

TODO: we have to discuss and decide how we solve that. A feew weeks ago we decided to postpone that decision and did not support openingHous in the first version.

### Image
http://xmlns.com/foaf/spec/#term_img
http://xmlns.com/foaf/spec/#term_thumbnail
http://xmlns.com/foaf/spec/#term_logo

### category/tag
https://www.w3.org/TR/vcard-rdf/#d4e813
Bei KVM steckt semantik hinter manchen tags, diese sollte man auflösen!

### License
https://schema.org/license

# Sample Entry
```
@prefix fairsync: <http://fairsync.naturzukunft.de/> .
@prefix person: <http://www.w3.org/ns/person#> .
@prefix schema: <https://schema.org/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

fairsync:12912983213423098721430ss40569 a foaf:Group;
  dcterms:Location fairsync:12912983213423098721430ss40569_location;
  schema:ContactPoint fairsync:12912983213423098721430ss40569_contactPoint;
  dcterms:identifier "12912983213423098721430ß40569";
  foaf:name "Mutsreman GmbH & Ko KG";
  foaf:homepage "http://example.org";
  owl:versionInfo "3";
  dcterms:description "This is a sample description of this sample organisation!";
  schema:foundingDate "2015-01-02" .

fairsync:12912983213423098721430ss40569_location a dcterms:Location;
  locn:Geometry fairsync:12912983213423098721430ss40569_geometry;
  locn:Address fairsync:12912983213423098721430ss40569_address .

fairsync:12912983213423098721430ss40569_geometry a locn:Geometry;
  schema:latitude 4.669561163368431E1;
  schema:longitude 7.619049236001004E0 .

fairsync:12912983213423098721430ss40569_address a locn:Address;
  locn:postCode "82633";
  locn:adminUnitL1 "Germany";
  locn:adminUnitL2 "BW";
  locn:postName "München";
  locn:addressArea "KluedoStrasse 12" .

fairsync:12912983213423098721430ss40569_contactPoint a schema:ContactPoint;
  schema:name "Mr. Mustermann";
  schema:email "max@mustremann.de";
  schema:telephone "089 62899835" .
```

# Integrating Karte von morgen
## OpenFairDB adapting
![Adapter 1](domain_diagrams/adapter1.png "Adapter 1")

- OpenFairDb has to provide an endpoint for each Resource responding with text/turtle.
- OpenFairDb has to implement a single load of all data to the RDF4J Server (using REST API)
- OpenFairDb has to take care, that the data iin the RDF4J Server is upToDate (using REST API)
- To be clarified: Are there some restrictions for Using SPARQL with that Adapter?
- Sure, it's not possible to use manipulating SPARQL queries. Attention: Can we be sure, that nobody is using it?

## Exteral adapter
![Adapter 2](domain_diagrams/Adapter2.png "Adapter 2")

- external service has to provide an endpoint for each Resource responding with text/turtle.
- external service has to implement a single load of all data to the RDF4J Server (using REST API)
- external service has to take care, that the data iin the RDF4J Server is upToDate (using REST API)
- external service has to communicate with openFairDB to keep syncronized (listening/polling)
- To be clarified: Are there some restrictions for Using SPARQL with that Adapter?
- Sure, it's not possible to use manipulating SPARQL queries. Attention: Can we be sure, that nobody is using it?

## Database replacement
![Database replacement](domain_diagrams/newKvmBackend.png "Database replacement")

- OpenFairDb has to replace it's database with a RDF4J Server (or another SPARQL supporting triple store) 
- Self hosted or RDF4J as a service
