# Specification

This project contains specifications that are created in the fairsync project. These specifications will have their own project starting in February 2021 so that we can enable a contributor process and create partner merge requests.

Before February 2021 the specs was maintained [here](https://git.fairkom.net/fairsync/maps/-/blob/master/specification/README.md)

!! This is a DRAFT version !!
